#!/usr/bin/env python

import pandas as pd
import numpy as np
import argparse

# Read command line and set args
parser = argparse.ArgumentParser(prog='region2pattern', description='Generate patterns given gene expressions')
parser.add_argument('-r', '--region_file',   nargs='?', help='Region read density file (CSV)')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.region_file or not args.output_file:
    parser.print_help()
    exit(1)

# Read input files
expr_df = pd.read_csv(args.region_file)

expr_df = expr_df.set_index(expr_df.columns[0])

# get line with 0 only
col0 = expr_df.apply(lambda row: row.sum(), axis=1) == 0

# get lines with 5 only
def is_5(row):
    for i in row:
        if i == 0.0:
            return False
        for j in row:
            if j == 0.0:
                return False
            elif (i <= j) and (i / j < 0.5):
                return False
    return True

col5 = expr_df.apply(lambda row: is_5(row), axis=1)

# get line not 5 or 0
colo = (col0 | col5) == False

# calculate best log value
minval = expr_df[expr_df > 0].min().min()
print(minval)
logoff = 10
while minval * logoff < 1:
    logoff *= 10
logoff *= 10
logoff = 1 / logoff
# Put in log
def log1(x):
    return np.log(x+logoff)
expr_df = expr_df.applymap(log1)

q_df = expr_df.copy()
q_df['min']=q_df.min(axis=1)
q_df['max']=q_df.max(axis=1)
q_df['q25']=q_df['min']+(q_df['max']-q_df['min'])/4
q_df['q50']=q_df['q25']+(q_df['max']-q_df['min'])/4
q_df['q75']=q_df['q50']+(q_df['max']-q_df['min'])/4

q_df = q_df[['q25','q50','q75']]

type_dict = {}
cols = expr_df.columns
for i in cols:
    is1 = expr_df[i] < q_df['q25']
    is2 = (expr_df[i] >= q_df['q25']) & (expr_df[i] < q_df['q50'])
    is3 = (expr_df[i] >= q_df['q50']) & (expr_df[i] < q_df['q75'])
    is4 = expr_df[i] > q_df['q75']
    expr_df.loc[is1,i] = 1
    expr_df.loc[is2,i] = 2
    expr_df.loc[is3,i] = 3
    expr_df.loc[is4,i] = 4
    type_dict[i] = int

expr_df = expr_df.astype(type_dict)
name = expr_df.index.name + "_Pattern"
expr_df[name] = 'unknown'
expr_df.loc[col0] = 0
expr_df.loc[col5] = 5

def to_pattern(row):
    row = row[:-1]
    row = ''.join(map(str, row))
    return row

expr_df[name] = expr_df.apply(lambda x: to_pattern(list(x)), axis=1)

expr_df.to_csv(args.output_file, index=True, doublequote=False)
