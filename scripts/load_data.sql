
-- Deleting previous entries of loader script
delete from DB.DBA.load_list;
-- see http://www.openlinksw.com/dataspace/dav/wiki/Main/VirtBulkRDFLoader
select 'Loading data...';
-- ld_dir (<folder with data>, <pattern>, <default graph if no graph file specified>)
ld_dir ('./output', '*.ttl', '');

rdf_loader_run();
